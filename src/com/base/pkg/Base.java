package com.pomclasses.pkg;

import java.util.List;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class HomePage
{
	@FindBy(id="tab-flight-tab")
	private WebElement flightsLink;

	@FindBy(id=("flight-origin"))
	private WebElement flyingFromTextField; 

	@FindBy(id=("flight-destination"))
	private WebElement flyingToTextField;

	@FindBy(id=("aria-option-0"))
	private WebElement firstFromCity;

	@FindBy(id=("aria-option-0"))
	private WebElement firstToCity;

	@FindBy (id=("flight-departing"))
	private WebElement departDateTextField;

	@FindBy(xpath=("//div[@id='flight-departing-wrapper']/div/div/div[2]/"
			+ "table/tbody//tr//td/button[@class != 'datepicker-cal-date disabled']"))
	private List <WebElement> activeDeparttureDateList;

	@FindBy (id=("flight-returning"))
	private WebElement ruturnDateTextField;

	@FindBy(xpath=("//div[@id='flight-returning-wrapper']/div/div/div[3]/"
			+ "table/tbody//tr//td/button[@class != 'datepicker-cal-date disabled']"))
	private List <WebElement> activeReturnDateList;

	@FindBy(id=("search-button"))
	private WebElement searchButton;

	WebDriverWait wait=null;

	public HomePage(WebDriver driver)
	{
		PageFactory.initElements(driver, this);
		wait=new WebDriverWait(driver, 20);
	}

	public void clickFlights() 
	{

		flightsLink.click();

		/*Select From City*/
		flyingFromTextField.click();
		flyingFromTextField.sendKeys("Pune");
		//Thread.sleep(2000);
		//firstFromCity.click();
		wait.until(ExpectedConditions.elementToBeClickable(firstFromCity)).click();


		/*Select To City*/
		flyingToTextField.click();
		flyingToTextField.sendKeys("Mumbai");
		wait.until(ExpectedConditions.elementToBeClickable(firstToCity)).click();


		/*Select Departure Date*/
		departDateTextField.click();
		activeDeparttureDateList.get(0).click();

		/*Select Return Date*/
		ruturnDateTextField.click();
		activeReturnDateList.get(2).click();

	}

	public void bookingExecution() 
	{
		clickFlights();
	}

	public void searchClick()
	{
		searchButton.click();
	}

	@FindBy(css="ul[class='utility-nav nav-group cf']>li[id*='-header']"
			+ ">a:not([id='primary-header-cruiseLegacy'])")
	private List<WebElement> homepageLinkList;

	String[] homePageLinksArr = { "Home", "Hotels", "Flights", "Packages",
			"Cars", "Vacation Rentals", "Cruises",
			"Deals", "Activities", "Discover", "Mobile", "Rewards" };

	public void homeLinks()
	{
		System.out.println("Total Links are : " + homepageLinkList.size());
		for (int i = 0; i < homepageLinkList.size(); i++) {

			Assert.assertEquals("Link text mismatches", homePageLinksArr[i], homepageLinkList.get(i).getText());
			String pagename = homepageLinkList.get(i).getText() + ".png";
			homepageLinkList.get(i).click();
			//takeScreenShot(driver, ".\\HomePageLinksScreenShots\\" + pagename);
		}
	}

}
